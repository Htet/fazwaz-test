<?php

use App\Http\Controllers\API\PropertyAPIController;
use Illuminate\Support\Facades\Route;

Route::get('/properties', [PropertyAPIController::class, 'index']);
