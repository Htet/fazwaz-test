<?php

namespace App\Repositories;

use App\Data\Property\PropertyListingFilterData;
use App\Models\Property;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class PropertyRepository
{
    private Property $model;
    private const DEFAULT_PAGE_SIZE = 25;

    public function __construct(Property $property)
    {
        $this->model = $property;
    }

    /**
     * @param PropertyListingFilterData $filterData
     * @return LengthAwarePaginator
     */
    public function getPaginatedListing(PropertyListingFilterData $filterData): LengthAwarePaginator
    {
        $query = $this->model->query()
            ->with(['location', 'photo'])
            ->where('sold', '=', false)
            ->orderBy('created_at', 'desc');

        if ($filterData->search) {
            $query->where(function ($query) use ($filterData) {
                $query->where('title', 'ilike', '%' . $filterData->search . '%')
                    ->orWhereHas('location', function ($query) use ($filterData) {
                        $query->where('province', 'ilike', '%' . $filterData->search . '%');
                    });
            });
        }

        if ($filterData->min_price) {
            $query->where('price', '>=', $filterData->min_price);
        }

        if ($filterData->max_price) {
            $query->where('price', '<=', $filterData->max_price);
        }

        if ($filterData->min_bedrooms) {
            $query->where('bedrooms', '>=', $filterData->min_bedrooms);
        } else if ($filterData->bedrooms) {
            $query->where('bedrooms', '=', $filterData->bedrooms);
        }

        if ($filterData->min_bathrooms) {
            $query->where('bathrooms', '>=', $filterData->min_bathrooms);
        } else if ($filterData->bathrooms) {
            $query->where('bathrooms', '=', $filterData->bathrooms);
        }

        if ($filterData->province) {
            $query->whereHas('location', function ($query) use ($filterData) {
                $query->where('province', '=', $filterData->province);
            });
        }

        if (!is_null($filterData->for_sale)) {
            $query->where('for_sale', '=', $filterData->for_sale);
        }

        if (!is_null($filterData->for_rent)) {
            $query->where('for_rent', '=', $filterData->for_rent);
        }

        return $query->paginate($filterData->page_size ?? self::DEFAULT_PAGE_SIZE);
    }
}
