<?php

namespace App\Http\Controllers\API;

use App\Data\Property\PropertyListingFilterData;
use App\Http\Controllers\Controller;
use App\Http\Requests\PropertyListingAPIRequest;
use App\Http\Resources\PropertyResource;
use App\Repositories\PropertyRepository;
use App\Traits\JsonResponseTrait;
use Illuminate\Http\JsonResponse;

class PropertyAPIController extends Controller
{
    use JsonResponseTrait;

    private readonly PropertyRepository $repository;

    public function __construct(PropertyRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(PropertyListingAPIRequest $request): JsonResponse
    {
        try {
            $result = $this->repository->getPaginatedListing(
                PropertyListingFilterData::from($request->validated())
            );

            return $this->successResponse(
                PropertyResource::collection($result)->response()->getData(true)
            );
        } catch (\Exception $e) {
            return $this->serverErrorResponse($e->getMessage());
        }
    }
}
