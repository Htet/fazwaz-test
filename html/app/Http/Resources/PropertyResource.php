<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PropertyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'price' => (float)$this->price,
            'formatted_price' => $this->currency_symbol . ' ' . number_format($this->price, 2), // '£ 1,000.00
            'bedrooms' => $this->bedrooms,
            'bathrooms' => $this->bathrooms,
            'area' => $this->area,
            'area_type' => $this->area_type,
            'property_type' => $this->property_type,
            'for_sale' => $this->for_sale,
            'for_rent' => $this->for_rent,
            'sold' => $this->sold,
            'currency' => $this->currency,
            'currency_symbol' => $this->currency_symbol,
            'photo' => $this->whenLoaded('photo'),
            'location' => $this->whenLoaded('location'),
        ];
    }
}
