<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;

trait JsonResponseTrait
{
    /**
     * Return a JSON response with a 200 status code.
     *
     * @param mixed|null $data
     * @param string $message
     * @return JsonResponse
     */
    protected function successResponse(mixed $data = null, string $message = 'Success'): JsonResponse
    {
        return response()->json([
            'status' => 'success',
            'message' => $message,
            'data' => $data,
        ], 200);
    }

    /**
     * Return a JSON response with a 201 status code.
     *
     * @param mixed|null $data
     * @param string $message
     * @return JsonResponse
     */
    protected function createdResponse(mixed $data = null, string $message = 'Created'): JsonResponse
    {
        return response()->json([
            'status' => 'success',
            'message' => $message,
            'data' => $data,
        ], 201);
    }

    /**
     * Return a JSON response with a 400 status code.
     *
     * @param string $message
     * @return JsonResponse
     */
    protected function badRequestResponse(string $message = 'Bad Request'): JsonResponse
    {
        return response()->json([
            'status' => 'error',
            'message' => $message,
        ], 400);
    }

    /**
     * Return a JSON response with a 401 status code.
     *
     * @param string $message
     * @return JsonResponse
     */
    protected function unauthorizedResponse(string $message = 'Unauthorized'): JsonResponse
    {
        return response()->json([
            'status' => 'error',
            'message' => $message,
        ], 401);
    }

    /**
     * Return a JSON response with a 403 status code.
     *
     * @param string $message
     * @return JsonResponse
     */
    protected function forbiddenResponse(string $message = 'Forbidden'): JsonResponse
    {
        return response()->json([
            'status' => 'error',
            'message' => $message,
        ], 403);
    }

    /**
     * Return a JSON response with a 404 status code.
     *
     * @param string $message
     * @return JsonResponse
     */
    protected function notFoundResponse(string $message = 'Not Found'): JsonResponse
    {
        return response()->json([
            'status' => 'error',
            'message' => $message,
        ], 404);
    }

    /**
     * Return a JSON response with a 422 status code.
     *
     * @param mixed|null $errors
     * @param string $message
     * @return JsonResponse
     */
    protected function validationErrorResponse(mixed $errors = null, string $message = 'Validation Error'): JsonResponse
    {
        return response()->json([
            'status' => 'error',
            'message' => $message,
            'errors' => $errors,
        ], 422);
    }

    /**
     * Return a JSON response with a 500 status code.
     *
     * @param string $message
     * @return JsonResponse
     */
    protected function serverErrorResponse(string $message = 'Server Error'): JsonResponse
    {
        return response()->json([
            'status' => 'error',
            'message' => $message,
        ], 500);
    }
}
