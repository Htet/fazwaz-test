<?php

namespace App\Data\PredefinedData;

use App\Data\LocationData;
use App\Data\PhotoData;
use Spatie\LaravelData\Data;

class PropertyJSONData extends Data
{
    public function __construct(
        public string $title,
        public string $description,
        public bool $for_sale,
        public bool $for_rent,
        public bool $sold,
        public int $price,
        public int $bedrooms,
        public int $bathrooms,
        public int $area,
        public string $area_type,
        public string $currency,
        public string $currency_symbol,
        public string $property_type,
        public ?PhotoData $photos,
        public ?LocationData $geo,
    ) {}
}
