<?php

namespace App\Data\Enum;

enum PropertyTypeEnum: string
{
    case house = 'house';

    case apartment = 'apartment';

    case condo = 'condo';

    case townhouse = 'townhouse';

    case land = 'land';

    case commercial = 'commercial';

    case villa = 'villa';

    public static function values(): array
    {
        return [
            self::house->value,
            self::apartment->value,
            self::condo->value,
            self::townhouse->value,
            self::land->value,
            self::commercial->value,
            self::villa->value,
        ];
    }
}
