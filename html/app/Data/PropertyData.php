<?php

namespace App\Data;

use Spatie\LaravelData\Data;

class PropertyData extends Data
{
    public function __construct(
        public string $title,
        public string $description,
        public bool   $for_sale,
        public bool   $for_rent,
        public bool   $sold,
        public int    $price,
        public int    $bedrooms,
        public int    $bathrooms,
        public int    $area,
        public string $area_type,
        public string $currency,
        public string $currency_symbol,
        public string $property_type,
        public ?int   $photo_id,
        public ?int   $location_id,
    )
    {
    }
}
