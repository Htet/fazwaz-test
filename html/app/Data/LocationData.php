<?php

namespace App\Data;

use Spatie\LaravelData\Data;

class LocationData extends Data
{
    public function __construct(
        public string $street,
        public string $province,
        public string $country,
    ) {}
}
