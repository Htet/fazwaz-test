<?php

namespace App\Data;

use Spatie\LaravelData\Data;

class PhotoData extends Data
{
    public function __construct(
        public string $search,
        public string $full,
        public string $thumb,
    ) {}
}
