<?php

namespace App\Data\Property;

use Spatie\LaravelData\Data;

class PropertyListingFilterData extends Data
{
    public function __construct(
        public ?string $search,
        public ?float  $min_price,
        public ?float  $max_price,
        public ?int    $min_bedrooms,
        public ?int    $bedrooms,
        public ?int    $min_bathrooms,
        public ?int    $bathrooms,
        public ?string $province,
        public ?bool   $for_sale,
        public ?bool   $for_rent,
    )
    {
    }
}
