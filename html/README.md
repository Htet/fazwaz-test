<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/11.x/installation)

Alternative installation is possible without local dependencies relying on [Docker](#docker). 

Clone the repository

    git clone <project-url>

Switch to the repo folder

    cd <project-dir>

Install all the dependencies using composer

    composer install

Install all the frontend dependencies using yarn

    yarn install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate



**TL;DR command list**

    git clone git@github.com:gothinkster/laravel-realworld-example-app.git
    cd laravel-realworld-example-app
    composer install
    yarn install
    cp .env.example .env
    php artisan key:generate
   

## Docker

To setup with docker, please run following command.

Up docker services

    ./vendor/bin/sail up -d

Access bash shell of organization dashboard

    docker exec -it <container-name> bash

**Make sure you set the correct database connection information before running the migrations** [Environment variables](#environment-variables)
This has to be run inside docker container

    php artisan migrate --seed

Reference: Laravel provide solution to work with docker using [Laravel Sail](https://laravel.com/docs/11.x/sail)

## Testing

Run following command to make sure project requestable.

    yarn dev

Run following command to check tests. 

    php artisan test

----------

# Code overview

## Folders

- `app` - Contains all the controllers and data structure
- `resources` - Contains the frontend related resources
- `config` - Contains all the application configuration files
- `database` - Contains database migrations, factories and seeders
- `routes` - Contains application routes

## Environment variables

- `.env` - Environment variables can be set in this file

***Note*** : You can quickly set the database information and other variables in this file and have the application fully working.

----------
