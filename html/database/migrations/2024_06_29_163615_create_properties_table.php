<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('description')->nullable();
            $table->boolean('for_sale')->default(false);
            $table->boolean('for_rent')->default(false);
            $table->boolean('sold')->default(false);
            $table->decimal('price', 10, 2)->default(0)->index('idx_price');
            $table->string('currency')->default('THB');
            $table->string('currency_symbol')->default('฿');
            $table->enum('property_type', ['house', 'apartment', 'condo', 'townhouse', 'land', 'commercial', 'villa']);
            $table->integer('bedrooms')->default(0)->index('idx_bedrooms');
            $table->integer('bathrooms')->default(0)->index('idx_bathrooms');
            $table->integer('area')->default(0)->index('idx_area');
            $table->string('area_type')->default('sqm');

            // attach photo to property
            $table->foreignId('photo_id')
                ->nullable()
                ->index('idx_photo_id')
                ->constrained('photos')
                ->nullOnDelete();

            // attach location to property
            $table->foreignId('location_id')
                ->nullable()
                ->index('idx_location_id')
                ->constrained('locations')
                ->nullOnDelete();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('properties');
    }
};
