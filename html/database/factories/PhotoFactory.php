<?php

namespace Database\Factories;

use App\Models\Photo;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Photo>
 */
class PhotoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'thumb' => $this->faker->imageUrl(640, 480, 'animals', true),
            'full' => $this->faker->imageUrl(1920, 1080, 'animals', true),
            'search' => $this->faker->imageUrl(640, 480, 'animals', true),
        ];
    }
}
