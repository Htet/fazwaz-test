<?php

namespace Database\Factories;

use App\Models\Location;
use App\Models\Photo;
use App\Models\Property;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Property>
 */
class PropertyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->sentence(4),
            'description' => $this->faker->paragraph(4),
            'price' => $this->faker->numberBetween(10000, 1000000),
            'bedrooms' => $this->faker->numberBetween(1, 10),
            'bathrooms' => $this->faker->numberBetween(1, 5),
            'area' => $this->faker->numberBetween(20, 1000),
            'area_type' => $this->faker->randomElement(['sqft', 'sqm']),
            'location_id' => Location::factory(),
            'photo_id' => Photo::factory(),
            'property_type' => $this->faker->randomElement(['house', 'apartment', 'condo', 'townhouse', 'land', 'commercial']),
            'for_sale' => $this->faker->boolean(50),
            'for_rent' => $this->faker->boolean(50),
            'sold' => false,
        ];
    }
}
