<?php

namespace Database\Seeders;

use App\Data\Enum\PropertyTypeEnum;
use App\Data\PredefinedData\PropertyJSONData;
use App\Models\Location;
use App\Models\Photo;
use App\Models\Property;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // if properties table is not empty, do not seed
        if (Property::query()->exists()) {
            return;
        }

        $properties_collection = $this->loadPropertiesJSONData();

        $properties_collection->chunk(20)->each(function (Collection $properties) {
            $properties->each(function (array $property) {
                if (!$this->isValidPropertyType($property['property_type'])) {
                    return;
                }

                $payload = PropertyJSONData::from($property);

                $this->createProperty($payload);
            });
        });
    }

    protected function createProperty(PropertyJSONData $payload): void
    {
        try {
            DB::beginTransaction();

            // create photo if exists
            if ($payload->photos) {
                $photo = Photo::query()->create(
                    $payload->photos->toArray()
                );
            }

            // create location if exists
            if ($payload->geo) {
                $location = Location::query()->create(
                    $payload->geo->toArray()
                );
            }

            // create property with photo and location
            Property::query()->create(
                $this->transformPropertyPayload($payload, $photo->id ?? null, $location->id ?? null)
            );

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage(), [
                'payload' => $payload->toArray(),
            ]);

            return;
        }
    }

    protected function loadPropertiesJSONData(): Collection
    {
        $properties_json = file_get_contents(
            $this->getPropertiesJSONPath()
        );

        $decoded_properties = json_decode($properties_json, true);

        return collect($decoded_properties);
    }

    protected function getPropertiesJSONPath(): string
    {
        return database_path('data/properties.json');
    }


    private function transformPropertyPayload(PropertyJSONData $property, ?int $photo_id, ?int $location_id): array
    {
        $data = $property->except('photos', 'geo', 'property_type');

        return collect($data->toArray())->merge([
            'property_type' => Str::of($property->property_type)->lower(),
            'photo_id' => $photo_id,
            'location_id' => $location_id,
        ])->toArray();
    }

    private function isValidPropertyType(string $property_type): bool
    {
        return in_array(
            Str::of($property_type)->lower(),
            PropertyTypeEnum::values()
        );
    }
}
