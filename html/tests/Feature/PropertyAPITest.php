<?php

use App\Models\Property;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class);

it('returns a successful response', function () {
    $response = $this->get('/');

    $response->assertStatus(200);
});

it('can see property that is in the database', function () {
    // add a property to the database using factory
    $property = Property::factory()->create([
        'title' => 'Test Property',
    ]);

    // visit index page
    $response = $this->get('/api/properties');

    // assert that the property is visible
    $response->assertJsonFragment([
        'title' => $property->title,
    ]);
});

it('can search valid properties via api', function () {
    // add two different title properties to the database using factory
    Property::factory()->count(25)->create();

    $property = Property::factory()->create([
        'title' => 'Test Property 1',
    ]);

    // search for the property using the api
    $response = $this->get('/api/properties?search=' . $property->title);

    // assert that the response is successful
    $response->assertStatus(200);

    // assert that the property is returned
    $response->assertJsonFragment([
        'title' => $property->title,
    ]);
});

it('cannot search properties that is not exist via api', function () {
    // create 25 properties
    Property::factory()->count(25)->make();

    // search non-existent property
    $response = $this->get('/api/properties?search=Non Existent Property');

    // assert that the response is successful
    $response->assertStatus(200);

    // assert that the property is not returned
    $response->assertJsonMissing([
        'title' => 'Non Existent Property',
    ]);
});

it('can filter properties via api', function () {
    // create 25 properties
    Property::factory()->count(25)->create();

    // create a property with a specific title, price, bedroom, bathroom
    $property = Property::factory()->create([
        'title' => 'Test Property',
        'price' => 100000,
        'bedrooms' => 3,
        'bathrooms' => 2,
    ]);

    // filter for the property using the api
    $response = $this->get('/api/properties?title=' . $property->title . '&price=' . $property->price . '&min_bedrooms=' . $property->bedrooms . '&min_bathrooms=' . $property->bathrooms);

    // assert that the property is returned
    $response->assertJsonFragment([
        'title' => $property->title,
        'price' => $property->price,
        'bedrooms' => $property->bedrooms,
        'bathrooms' => $property->bathrooms,
    ]);
});

it('can paginate properties via api', function () {
    // create 50 properties
    Property::factory()->count(50)->make();

    // paginate for the property using the api
    $response = $this->get('/api/properties?page=2');

    // assert that the property is returned
    $response->assertJsonFragment([
        'current_page' => 2,
    ]);
});


