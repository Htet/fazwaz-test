import React from "react";
import {Property} from "@/types";
import {Card} from "@/components/ui/card";
import {Badge} from "@/components/ui/badge";
import {Label} from "@/components/ui/label";

interface PropertyCardProps {
    property: Property;
}

const PropertyCard = ({property}: PropertyCardProps) => {
    return (
        <Card className='flex gap-x-4 items-center p-2'>
            <img src={property.photo?.thumb} alt={property.title} className="w-150 h-full object-cover object-center"/>
            <div className="space-y-1">
                <h2 className="text-xl font-bold">{property.title}</h2>
                <Badge>
                    {property.property_type}
                </Badge>
                <p className="text-gray-500">{property.location?.province}</p>
                <Label className="text-gray-500">{property.formatted_price}</Label>
            </div>
        </Card>
    )
}

export default PropertyCard;
