import {useEffect, useState} from "react";
import axios, {AxiosResponse} from "axios";
import {Meta, Property, PropertyListingResponse} from "@/types";

export default function useGetPropertyListing() {
    const [properties, setProperties] = useState<Array<Property>>([]);
    const [loading, setLoading] = useState(true);
    const [search, setSearch] = useState('');
    // state for pagination
    const [page, setPage] = useState(1);
    const [meta, setMeta] = useState<Meta>({
        current_page: 1,
        from: 1,
        last_page: 1,
        links: [],
        total: 0
    });

    // fetch on load
    useEffect(() => {
        fetchProperties();
    }, [page]);

    // fetch properties
    const fetchProperties = async () => {
        setLoading(true);
        try {
            let url = `/api/properties?page=${page}`;

            if (search) {
                url += `&search=${search}`;
            }

            const response = await axios.get<any, AxiosResponse<PropertyListingResponse>>(url);

            setProperties(response.data.data.data);
            setMeta(response.data.data.meta);
        } catch (e) {
            console.error(e);
        } finally {
            setLoading(false);
        }
    }

    const canPrevious = meta.current_page > 1;

    const canNext = meta.current_page < meta.last_page;

    return {
        loading,
        properties,
        meta,
        search,
        fetchProperties,
        setPage,
        setSearch,
        canPrevious,
        canNext
    }
}
