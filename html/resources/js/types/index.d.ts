import {Config} from 'ziggy-js';

export type PageProps<T extends Record<string, unknown> = Record<string, unknown>> = T & {
    ziggy: Config & { location: string };
};

export type Photo = {
    id: number;
    thumb: string;
    full: string;
    search: string;
}

export type Location = {
    id: number;
    street?: string;
    province: string;
    country: string;
}

export type Property = {
    id: number;
    title: string;
    description?: string;
    price: number;
    formatted_price: string;
    for_sale: boolean;
    for_rent: boolean;
    sold: boolean;
    area: number;
    area_type: string;
    bedrooms: number;
    bathrooms: number;
    property_type: string;
    currency: string;
    currency_symbol: string;
    photo_id: number | null;
    location_id: number | null;
    photo?: Photo;
    location?: Location;
}

export type MetaLink = {
    url: string | null;
    label: string;
    active: boolean;
}

export type Meta = {
    current_page: number;
    from: number;
    last_page: number;
    links: MetaLink[];
    path?: string;
    per_page?: number;
    to?: number;
    total: number;
}

export type PropertyListingResponse = {
    data: {
        data: Property[];
        links: {
            first: string;
            last: string;
            prev: string | null;
            next: string | null;
        }
        meta: Meta;
    }
}
