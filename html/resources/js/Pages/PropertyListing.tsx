import {Head} from '@inertiajs/react';
import useGetPropertyListing from "@/hooks/useGetPropertyListing";
import PropertyCard from "@/components/property/card";
import {Fragment} from "react";
import {Input} from "@/components/ui/input";
import {Button} from "@/components/ui/button";
import {
    Pagination,
    PaginationContent,
    PaginationItem,
    PaginationNext,
    PaginationPrevious
} from "@/components/ui/pagination";

export default function PropertyListing() {
    const {
        loading,
        properties,
        search,
        canPrevious,
        canNext,
        setPage,
        fetchProperties,
        setSearch
    } = useGetPropertyListing();

    const onPrev = () => {
        if (!canPrevious) {
            return;
        }

        setPage((prev) => prev - 1);
    }

    const onNext = () => {
        if (!canNext) {
            return;
        }

        setPage((prev) => prev + 1);
    }

    return (
        <>
            <Head title="PropertyListing"/>

            {/* div container to show listing using tailwind css */}
            <div className="container mx-auto p-4 space-y-12">
                <h1 className="text-2xl font-bold">Property Listing</h1>

                <div className='grid grid-cols-6 gap-x-2'>
                    <Input
                        type="text"
                        placeholder="Search..."
                        className='col-span-5'
                        value={search}
                        onChange={(e) => {
                            setSearch(e.target.value);
                        }}
                    />

                    <Button onClick={fetchProperties}>Search</Button>
                </div>

                <div className="space-y-6">
                    {loading && (
                        <div>Loading...</div>
                    )}

                    {properties.length === 0 && !loading && (
                        <div>No properties found</div>
                    )}

                    {!loading && properties.map((property) => (
                        <Fragment key={property.id}>
                            <PropertyCard property={property}/>
                        </Fragment>
                    ))}
                </div>

                <Pagination>
                    <PaginationContent>
                        <PaginationItem>
                            <PaginationPrevious
                                className={canPrevious ? 'cursor-pointer' : 'cursor-not-allowed opacity-75'}
                                onClick={(e) => {
                                    e.preventDefault();
                                    onPrev();
                                }}
                            />
                        </PaginationItem>
                        <PaginationItem>
                            <PaginationNext
                                className={canNext ? 'cursor-pointer' : 'cursor-not-allowed opacity-75'}
                                onClick={(e) => {
                                    e.preventDefault();
                                    onNext();
                                }}
                            />
                        </PaginationItem>
                    </PaginationContent>
                </Pagination>
            </div>
        </>
    );
}
