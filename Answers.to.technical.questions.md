##### How long did you spend on the coding test? What additional features would you consider implementing if you had more time?\
On Saturday, I spent a few hours thinking about the strategy and tech stack I would like to use for this test and began setting up and implementing the data structure. I found that the setup process was a bit time-consuming which is a mistake from my end. Overall, I think it was around up to 3 hours for implementation. I did miss some of the feature in the readme file to implement and I hope you understand.
Regarding additional features, I am keen to use some form of frontend query caching and backend caching to optimize performance for production and some sorts of filtering by property type.

##### Describe a security best practice you would implement in this application to protect the API.\
Normally for an API that use a lot of resource, I would do some rate limiting and technology like cloudflare to sit between requester and actual server. If it's for an authenticated API routes, I would make sure authorziation flow is implemented correctly.

##### Explain how you would approach optimizing the performance of the API for handling large amounts of property data.\
I would consider caching ( both frontend and backend ) and query optimization if possible. 

##### How would you track down a performance issue in production? Have you ever had to do this? If so, please describe the experience.\
I would add some sort error logging tool to capture the error context like sentry or laravel telescope. Base on the provided context in those logging tool, I would solve the problem. One of my interesting experience was the time when I was writing the web page to display in mobile app which they used deep link to display. After delivering it to production for a weeks, I receive a production issue that there's a blank screen issue in the web page that used to display in mobile app. 
It was a bit hard to debug as it's working fine when you called it from any browser and after a few deep research, there's was a problem with browser that used to display web page which didn't meet minimun requirement to display web page like vue.js. 